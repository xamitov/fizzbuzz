# FizzBuzz

---
This is a FizzBuzz app, where user can log in to the app and increment fizzbuzz value. In fizzbuzz game any number divisible by three is replaced by the word fizz and any number divisible by five by the word buzz. Numbers divisible by 15 become fizzbuzz.

---
### Instructions:

In order to log in use this email and password (saved in database):
- email: johndoe@usf.edu
- password: password

---
### The following functionalities are completed:

- [x] User can log in to the app. 
- [x] User can log out of the app. 
- [x] User can increment the value of fizzbuzz. 
- [x] User sees the incremented value. 
- [ ] User's fizzbuzz value saved to database.

---
### Video Walkthrough:

Here's a walkthrough of implemented user stories:

<img src='https://gitlab.com/xamitov/fizzbuzz/-/raw/7d50e28de3ad9fe5bc47d3166577c530d664a06d/fizzbuzz.gif' title='Video Walkthrough' width='500' alt='Video Walkthrough' />

---

### Difficulties:

While working on the app I faced many difficulties. Mainly with AJAX. At first, I tried to work with AJAX but did not have enough experience with it. So, I decided to use Firebase. I implemented authentication with Firebase. Unfortunately, Fizzbuzz value cannot be saved in database, as I did not finish that part. After working on this app, I realized I should improve my backend skills, which I am currently working on.
