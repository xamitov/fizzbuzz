// import {getDatabase, ref, set, get, child, update, remove}
// from "https://www.gstatic.com/firebasejs/4.8.1/firebase-database.js";

var incrementElement = document.getElementById("increment");
var invisibleElement = document.getElementById("invisible");

// const db = getDatabase();

firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
    // User is signed in.

    document.getElementById("user_div").style.display = "block";
    document.getElementById("login_div").style.display = "none";

    var user = firebase.auth().currentUser;

    if(user != null){

      var email_id = user.email;
      document.getElementById("user_para").innerHTML = "Welcome " + email_id;

    }

  } else {
    // No user is signed in.

    document.getElementById("user_div").style.display = "none";
    document.getElementById("login_div").style.display = "block";

  }
});

function login(){

  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;

  firebase.auth().signInWithEmailAndPassword(userEmail, userPass).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);

    // ...
  });


  // const dbref = ref(db);

  // get(child(dbref, "Users/" + "khamitov527")).then((snapshot)=>{
  //   if(snapshot.exists()){
  //     incrementElement.value = snapshot.val().incrementValue;
  //     invisibleElement.value = snapshot.val().invisibleValue;
  //   }
  //   else{
  //     alert("Data not found.");     
  //   }
  // }).catch((error)=>{
  //   alert("Unsuccessful, new error " + error);
  // });

}

function logout(){
  firebase.auth().signOut();


  // set(ref(db, "Users/" + "khamitov527"),{
  //   incrementValue: incrementElement.value,
  //   invisibleValue: invisibleElement.value
  // })
  // .then(()=>{
  //   alert("Data stored successfully.");
  // })
  // .catch((error)=>{
  //   alert("Unsuccessful, error " + error);
  // });
}


function incrementValues(){

  var value = invisibleElement.innerHTML;

  value++;

  if(value % 15 === 0){
      document.getElementById("increment").innerHTML = "FizzBuzz";
      document.getElementById("invisible").innerHTML = value;
  }
  else if(value % 3 === 0){
      document.getElementById("increment").innerHTML = "Fizz"
      document.getElementById("invisible").innerHTML = value;
  }
  else if(value % 5 === 0){
      document.getElementById("increment").innerHTML = "Buzz"
      document.getElementById("invisible").innerHTML = value;
  }
  else{
      document.getElementById("increment").innerHTML = value;
      document.getElementById("invisible").innerHTML = value;
  }
}